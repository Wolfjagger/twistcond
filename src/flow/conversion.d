module flow.conversion;

import dsfml.system.vector2;
import dsfml.system.vector3;
import deltali.tensor.vec;



Vector2i deltaToDSFMLVec2I(Vec2I vec) {
    return Vector2i(vec.x, vec.y);
}

Vec2I dsfmlToDeltaVec2I(Vector2i vec) {
    return Vec2I(vec.x, vec.y);
}

Vec2I dsfmlToDeltaVec2I(Vector2u vec) {
    return Vec2I(vec.x, vec.y);
}

Vector2f deltaToDSFMLVec2F(Vec2F vec) {
    return Vector2f(vec.x, vec.y);
}

Vec2F dsfmlToDeltaVec2F(Vector2f vec) {
    return Vec2F(vec.x, vec.y);
}
