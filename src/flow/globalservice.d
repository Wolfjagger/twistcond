module flow.globalservice;

import std.exception;
import graphics.display : Display;
import graphics.resolution : Resolution;
import world.world : World;



static private {

    World currentWorld = null;

    void checkWorld() {
        if(!currentWorld) throw new Exception("No world set.");
    }

}

static public {

    void setCurrentWorld(World world) {
        currentWorld = world;
    }

    const(World) getCurrentWorld() {
        checkWorld();
        return currentWorld;
    }

    const(Display) getCurrentDisplay() {
        checkWorld();
        return currentWorld.display;
    }

    Resolution getCurrentResolution() {
        checkWorld();
        auto disp = currentWorld.display;
        return disp.resolution;
    }

}
