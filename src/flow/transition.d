module flow.transition;

import graphics.resolution;
import world.world;



static {

private:

    World currentWorld;
    
public:

    void startGame() {

        auto resolution = Resolution(1366, 768);
        WorldInit init;
        init.worldType = WorldType.Arena;
        init.res = &resolution;
        currentWorld = createWorld(init);

    }

    bool isValidGame() {
        return currentWorld.isValid;
    }
    
    void updateGame() {

        currentWorld.update();
        currentWorld.drawScreen();

    }

}
