module util.dsfmlextra;

import std.math : sqrt;
import dsfml.system.vector2;



float normSqu(Vector2f vec) {
    return vec.x*vec.x + vec.y*vec.y;
}

float norm(Vector2f vec) {
    return sqrt(vec.normSqu);
}

Vector2f unitVec(Vector2f vec) {
    auto norm = vec.norm;
    return Vector2f(vec.x/norm, vec.y/norm);
}
