module shapes.position;

public import deltali.tensor.vec;

import std.math : abs;
import dsfml.system.vector2;
import dsfml.graphics.renderwindow;
import graphics.resolution;



class Position {

private:

    immutable float minX, maxX, minY, maxY;
    immutable Vec2F center;
    Vec2F m_vec;

public:

    @property {
        Vec2F vec() const { return m_vec; }
        void vec(Vec2F newVec) { m_vec = newVec; }
    }

public:

    this(float minX, float maxX,
         float minY, float maxY,
         Vec2F vec)
    in {
        assert(0 <= minX && minX < maxX && maxX <= 1);
        assert(0 <= minY && minY < maxY && maxY <= 1);
        assert(minX <= vec.x && vec.x <= maxX);
        assert(minY <= vec.y && vec.y <= maxY);
    } body {

        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.center = Vec2F(minX+maxX, minY+maxY)/2;
        this.vec = vec;

    }

    this(Vec2F vec) {
        this(0, 1, 0, 1, vec);
    }

    this(float x, float y) {
        this(Vec2F(x,y));
    }

    this(typeof(this) other) {
        this(other.minX, other.maxX, other.minY, other.maxY,
             other.vec);
    }

    this(float minX, float maxX, float minY, float maxY) {
        this(minX, maxX, minY, maxY, Vec2F(minX, minY));
    }

    this() {
        this(0, 1, 0, 1);
    }



    Vector2f windowView() const {
        return getCurrentResolution().windowPos(vec);
    }

    Vec2F physicalView() const {
        return vec;
    }



    void setFromWindow(Vector2i windowPos) {
        vec = getCurrentResolution().physicalPos(windowPos);
        constrainToBounds();
    }



    Position opBinary(string op)(Position other) {
        Vec2F outVec = mixin("vec "~op~" other.vec");
        outVec.constrainToBounds();
        return outVec;
    }

    static float distSqu(Position a, Position b) {
        return (b.vec-a.vec).normSqu();
    }
    float distSqu(Position b) {
        return Position.distSqu(this, b);
    }

    static float dist(const Position a, const Position b) {
        return (b.vec-a.vec).norm();
    }
    float dist(const Position b) const {
        return Position.dist(this, b);
    }



private:

    void constrainToBounds() {

        if(vec.x < minX || vec.x > maxX ||
           vec.y < minY || vec.y > maxY) {

            auto diffVec = vec - center;
            auto unitVec = diffVec.unitVec();
            if(abs(diffVec.x)/(maxX-minX) > abs(diffVec.y)/(maxY-minY)) {
                vec = center + unitVec * (maxX-minX)/(2*abs(unitVec.x));
            } else {
                vec = center + unitVec * (maxY-minY)/(2*abs(unitVec.y));
            }

        }

    }

}
