module shapes.viscurve;

public import deltali.shape : BoundaryTopo;

import std.algorithm.mutation : reverse;
import std.exception;
import std.stdio : writeln;
import std.typecons;
import dsfml.graphics;
import deltali.shape : Differentiable, ICurve;
import deltali.tensor.vec : Vec2F;
import flow.conversion : deltaToDSFMLVec2F;
import graphics.resolution;
import shapes.position;
import util.dsfmlextra;



// Add "isVisible" flag, to draw or not?

class VisCurve(BoundaryTopo boundaryTopo) : Drawable {

public:

    alias TCurve = ICurve!(boundaryTopo, Differentiable.Tangents);

    struct Data {
        Color color;
        float thickness;
        bool capStart;
        bool capEnd;
    }

private:

    TCurve m_curve;
    Data data;

    Flag!"dirty" dirty = Yes.dirty;

    VertexArray vertArray = void;
    //TODO: Texture

public:

    @property {
        const(TCurve) curve() const { return m_curve; }
        void curve(TCurve value) {
            dirty = Yes.dirty;
            m_curve = value;
        }
        const(Color) color() const { return data.color; }
        void color(Color value) {
            if(data.color != value) {
                dirty = Yes.dirty;
                data.color = value;
            }
        }
        const(float) thickness() const { return data.thickness; }
        void thickness(float value) {
            dirty = Yes.dirty;
            data.thickness = value;
        }
    }

public:

    //TODO: Allow for array of colors/thicknesses: dependent
    // on curve's indices, arclength, or fraction of the curve.
    this(TCurve baseCurve, Data curveData) {
        
        curve = baseCurve;
        data = curveData;
        
    }
    
    this(TCurve baseCurve,
         Color curveColor, float outlineThickness,
         bool capStart = false, bool capEnd = false) {

        Data curveData = { curveColor, outlineThickness, capStart, capEnd };
        this(baseCurve, curveData);

    }



    override void draw(RenderTarget renderTarget, RenderStates renderStates) {

        if(dirty) calcVertArray();
        renderTarget.draw(vertArray, renderStates);

    }

    void setDirty() {
        dirty = Yes.dirty;
    }



private:

    void calcVertArray() {

        try {
            tryCalcVertArray(color, thickness);
            dirty = No.dirty;
        } catch(Exception e) {
            writeln(e.msg);
            vertArray = null;
        }

    }

    void tryCalcVertArray(Color color, float Thickness) {

        if(curve.numPts() < 2)
            throw new Exception("Too few pts in curve array.");

        static immutable auto smallLength = 0.001f;
        alias conv = deltaToDSFMLVec2F;

        // Need to be careful of near-zero-length spans, at least for now

        Vertex[] verts;
        verts.reserve(3*curve.numPts());

        static if(boundaryTopo == BoundaryTopo.Open) {
            if(data.capStart) {
                auto posStart = scoped!Position(curve.pts().front);
                auto tangentStart = curve.ptTangents().front;
                verts ~= calcRoundCapVerts!(SE.Start)(posStart, -tangentStart);
            }
        }

        for(auto idx = 0; idx<curve.numPts(); ++idx) {

            auto pos = scoped!Position(curve.pts()[idx]);
            auto tangent = curve.ptTangents()[idx];
            auto wTangent = getCurrentResolution().windowVec(tangent);
            auto perpLeft = Vector2f(-wTangent.y, wTangent.x).unitVec;
            perpLeft *= thickness/2;
            auto perpRight = -perpLeft;

            //TODO: Add extra if tangent difference past a certain threshold (90?)
            verts ~= Vertex(pos.windowView()+perpLeft, color);
            verts ~= Vertex(pos.windowView()+perpRight, color);

        }

        static if(boundaryTopo == BoundaryTopo.Open) {
            if(data.capEnd) {
                auto posEnd = scoped!Position(curve.pts().back);
                auto tangentEnd = curve.ptTangents().back;
                verts ~= calcRoundCapVerts!(SE.End)(posEnd, tangentEnd);
            }
        }

        static if(boundaryTopo == BoundaryTopo.Closed) {

            // Close off curve
            verts ~= verts[0];
            verts ~= verts[1];

        }
        
        if(!vertArray || vertArray.getVertexCount() != verts.length) {
            vertArray = new VertexArray(PrimitiveType.TrianglesStrip,
                                        cast(uint) verts.length);
        }
        for(auto idx=0; idx<verts.length; ++idx) vertArray[idx] = verts[idx];

    }



    private enum SE { Start, End }
    Vertex[] calcRoundCapVerts(SE startOrEnd)(Position pos, Vec2F capDir) {

        auto vTan = getCurrentResolution().windowVec(capDir).unitVec;
        vTan *= thickness/2;

        auto vPerp = Vector2f(vTan.y, -vTan.x);

        static auto numCapPts = 6;

        auto verts = new Vertex[numCapPts];
        auto idx = 0;

        if((numCapPts & 1) != 0) {
            verts[idx++] = Vertex(pos.windowView() + vTan, color);
        }

        for(auto i=0; i<numCapPts/2; ++i) {
            auto th = PI/(numCapPts+3)*(i+1);
            float c = cos(th);
            float s = sin(th);
            auto vL = Vector2f(c*vTan.x + s*vPerp.x, c*vTan.y + s*vPerp.y);
            auto vR = Vector2f(c*vTan.x - s*vPerp.x, c*vTan.y - s*vPerp.y);
            verts[idx++] = Vertex(pos.windowView() + vL, color);
            verts[idx++] = Vertex(pos.windowView() + vR, color);
        }

        static if(startOrEnd == SE.End) verts.reverse();

        return verts;

    }

}
