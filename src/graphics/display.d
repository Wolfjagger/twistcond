module graphics.display;

public import graphics.resolution;

import std.container.array : Array;
import dsfml.graphics;



class Display {

private:

	RenderWindow window;
	Array!Drawable drawables;

    Resolution res;

public:

    @property {

        const(RenderWindow) renderWindow() const {
            return window;
        }

        Resolution resolution() const {
            return res;
        }

    }

public:

    this(RenderWindow renderWindow) {

        window = renderWindow;
        drawables.length = 0;
        res = Resolution(window.getSize());

    }



    void draw() {

		Event event;

		while(window.pollEvent(event)) {
			if(event.type == event.EventType.Closed) window.close();
		}

		window.clear();

        foreach(Drawable drawable; drawables) {
            window.draw(drawable);
        }

		window.display();

	}



    void attach(Drawable drawable) {
        drawables.insertBack(drawable);
    }

    void detach(Drawable drawable) {
        Array!Drawable newDrawables;
        foreach(elemDrawable; drawables) {
            if(drawable != elemDrawable) newDrawables ~= elemDrawable;
        }
        drawables = newDrawables;
    }

    void clear() {
        drawables.clear;
    }

    bool isOpen() const { // Should be const, except Window.isOpen is non-const!
        auto mutableWindow = cast(RenderWindow) window;
		return mutableWindow.isOpen();
	}

}



static {

    Display createFullscreenDisp() {
        auto fullscreenVM = VideoMode.getDesktopMode();
        auto window = new RenderWindow(fullscreenVM,
                                       "Twisted Connections",
                                       Window.Style.None);
        return new Display(window);
    }

    Display createWindowDisp(Resolution res) {
        auto resVec = res.winRes;
        auto windowVM = VideoMode(resVec.x, resVec.y);
        auto window = new RenderWindow(windowVM,
                                       "Twisted Connections");
        return new Display(window);
    }

}
