module graphics.resolution;

public import dsfml.system.vector2;
public import flow.globalservice : getCurrentResolution;

import deltali.tensor.vec;
import flow.conversion;



struct Resolution {

    immutable Vec2I winRes;

public:

    this(Vec2I windowResolution) {
        winRes = windowResolution;
    }

    this(Vector2i dsfmlRes) {
        winRes = dsfmlToDeltaVec2I(dsfmlRes);
    }

    this(Vector2u dsfmlRes) {
        winRes = dsfmlToDeltaVec2I(dsfmlRes);
    }

    this(uint width, uint height) {
        winRes = Vec2I(width, height);
    }



    Vector2f windowPos(Vec2F physicalPos) const {
        auto x = winRes.x*physicalPos.x;
        auto y = winRes.y*(1-physicalPos.y);
        return Vector2f(x, y);
    }

    Vector2f windowVec(Vec2F physicalVec) const {
        auto x = winRes.x*physicalVec.x;
        auto y = -winRes.y*physicalVec.y;
        return Vector2f(x, y);
    }

    Vec2F physicalPos(Vector2f windowPos) const {
        auto x = windowPos.x/winRes.x;
        auto y = 1-windowPos.y/winRes.y;
        return Vec2F(x, y);
    }

    Vec2F physicalVec(Vector2f windowVec) const {
        auto x = windowVec.x/winRes.x;
        auto y = -windowVec.y/winRes.y;
        return Vec2F(x, y);
    }

    Vec2F physicalPos(Vector2i windowPos) const {
        return physicalPos(cast(Vector2f) windowPos);
    }

    Vec2F physicalVec(Vector2i windowVec) const {
        return physicalVec(cast(Vector2f) windowVec);
    }

}
