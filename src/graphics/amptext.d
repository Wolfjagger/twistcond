module graphics.amptext;

import deltali.tensor.vec;
import dsfml.graphics;
import dsfml.system.vector2;
import shapes.position;



class AmpText : Drawable {

public:

    enum Size {
        Small, Med, Large
    }

    enum Align {
        Left, Center, Right
    }

private:

    Text text;
    static Font font;

    static this() {

        string fontFilename = "LinBioLinum_R.otf";
//        string fontFilename = "OpenSans-Regular.ttf";
        font = new Font;
        auto success = font.loadFromFile(fontFilename);
        assert(success == true);

    }

public:

    this(dstring str, const(Position) pos, Color color,
         Size size = Size.Med, Align alignment = Align.Center,
         Text.Style style = Text.Style.Regular) {

        text = new Text();

        text.setFont(font);

        final switch(size) {
            case Size.Small:
                text.setCharacterSize(32);
                break;
            case Size.Med:
                text.setCharacterSize(64);
                break;
            case Size.Large:
                text.setCharacterSize(96);
                break;
        }

        text.setColor(color);
        text.setStyle(style);

        text.setString(str);

        auto bounds = text.getGlobalBounds();
        auto xDiff = -0.5f*bounds.width; // Should be width of text string, not bounding box
        auto yDiff = -bounds.height;
        text.position = pos.windowView + Vector2f(xDiff, yDiff);

    }

    override void draw(RenderTarget renderTarget, RenderStates renderStates) {

        text.draw(renderTarget, renderStates);

    }

}
