module world.world;

public import dsfml.graphics : Drawable, Mouse;
import flow.globalservice : setCurrentWorld;
import graphics.display;
import shapes.position;



abstract class World {

protected:

    Display disp;
    static Position mousePos;

public:

    @property {
        const(Display) display() const {
            return disp;
        }
        bool isValid() const {
            return disp.isOpen();
        }
    }

protected:

    this() {
        disp = createFullscreenDisp();
    }

    this(Resolution res) {
        disp = createWindowDisp(res);
    }

    this(Resolution* res) {
        if(res) this(*res);
        else this();
    }

package:

    void init() {

        mousePos = new Position();
        mousePos.setFromWindow(Mouse.getPosition(disp.renderWindow));

        doInit();

    }

public:

    void update() {
        mousePos.setFromWindow(Mouse.getPosition(disp.renderWindow));
        doUpdate();
    }

    void drawScreen() {
        preDraw();
        disp.draw();
        postDraw();
    }



    void attach(Drawable drawable) {
        disp.attach(drawable);
    }

    void attach(Drawable[] drawables) {
        foreach(drawable; drawables) attach(drawable);
    }


    void detach(Drawable drawable) {
        disp.detach(drawable);
    }

    void detach(Drawable[] drawables) {
        foreach(drawable; drawables) detach(drawable);
    }



    static const(Position) mousePosition() {
        return mousePos;
    }



protected:

    abstract void doInit();
    abstract void doUpdate();

    abstract void preDraw();
    abstract void postDraw();

}



enum WorldType {
    Arena
}

struct WorldInit {
    WorldType worldType;
    Resolution* res;
}

import world.arena.arena;



static {

private:

    World currentWorld;

public:

    World createWorld(WorldInit worldInit) {

        switch(worldInit.worldType) {
            case WorldType.Arena:
                currentWorld = new Arena(worldInit.res);
                break;
            default:
                assert(0);
        }

        setCurrentWorld(currentWorld);
        currentWorld.init();

        return currentWorld;

    }

}
