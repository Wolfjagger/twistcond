module world.arena.arena;

public import world.world;

import std.typecons : Nullable;
import dsfml.graphics.color;
import deltali.shape.line;
import deltali.util.common_types;
import graphics.amptext;
import graphics.resolution : Resolution;
import shapes.position;
import shapes.viscurve;
import world.arena.boundary;
import world.arena.camp;



struct ArenaEnv {
    Boundary boundary;
    Camp!(LR.Left) leftCamp;
    Camp!(LR.Right) rightCamp;
    uint boundaryThickness = 20;
    uint chordThickness = 20;
}

class Arena : World {

private:

    ArenaState state;
    ArenaEnv env;

public:

	this() {
        super();
	}

	this(Resolution res) {
        super(res);
	}

    this(Resolution* res) {
        super(res);
    }



protected:

    override void doInit() {

        state = new ArenaStateDefault(this);

        env.boundary = new Boundary(env.boundaryThickness);
        env.leftCamp = new Camp!(LR.Left)
            (env.boundary, [1,2,3], env.chordThickness);
        env.rightCamp = new Camp!(LR.Right)
            (env.boundary, [3,2,1], env.chordThickness);

        foreach(nub; env.leftCamp.nubs) attach(nub);
        foreach(nub; env.rightCamp.nubs) attach(nub);

        attach(env.boundary.visual);

        foreach(name; env.leftCamp.visNames) attach(name);
        foreach(name; env.rightCamp.visNames) attach(name);

    }

    override void doUpdate() {

        state = state.update();

    }

    override void preDraw() { }
    override void postDraw() { }

}



private abstract class ArenaState {

    Arena arena;

    this(Arena owner) {
        arena = owner;
    }

    ArenaState update() {

        doUpdate();

        auto nextState = transition();
        if(nextState.isNull()) {
            return this;
        } else {

            detach();
            nextState.attach();
            return nextState.get();

        }

    }



protected:

    // Nullable when that doesn't fail unit tests
    Nullable!ArenaState transition();

    void attach();

    void doUpdate();

    void detach();

}



private class ArenaStateDefault : ArenaState {

    this(Arena owner) {
        super(owner);
    }



protected:

    override Nullable!ArenaState transition() {

        auto outState = Nullable!ArenaState();

        if(Mouse.isButtonPressed(Mouse.Button.Left)) {
            outState = new ArenaStateDrawing(arena);
        }

        return outState;

    }

    override void attach() {}

    override void doUpdate() {}

    override void detach() {}

}



private class ArenaStateDrawing : ArenaState {

    Line!(Differentiable.Tangents) mouseLine;
    VisCurve!(BoundaryTopo.Open) visMouseLine;

    this(Arena owner) {
        super(owner);
    }



protected:

    override Nullable!ArenaState transition() {

        auto outState = Nullable!ArenaState();

        if(!Mouse.isButtonPressed(Mouse.Button.Left)) {
            outState = new ArenaStateDefault(arena);
        }

        return outState;

    }

    override void attach() {

        mouseLine = new Line!(Differentiable.Tangents)
            (Vec2F(0.5f, 0.5f), arena.mousePosition.physicalView);
        visMouseLine = new VisCurve!(BoundaryTopo.Open)
            (mouseLine, Color.Cyan, arena.env.chordThickness, true, true);

        arena.attach(visMouseLine);

    }

    override void doUpdate() {

        updateLineEnd();

        auto line = cast(ICurve!(BoundaryTopo.Open, Differentiable.Tangents))mouseLine;
        auto start = line.start;
        auto end = line.end;

        if(arena.env.boundary.intersects(start, end)) {
            arena.env.boundary.visual.color = Color.Red;
        } else {
            arena.env.boundary.visual.color = Color.Blue;
        }

    }

    override void detach() {

        arena.env.boundary.visual.color = Color.Blue;
        arena.detach(visMouseLine);

    }



private:

    void updateLineEnd() {

        mouseLine.end = arena.mousePosition.physicalView;
        visMouseLine.setDirty();

    }

}
