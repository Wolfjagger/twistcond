module world.arena.camp;

import std.container.array;
import std.conv : dtext;
import std.algorithm.iteration : each;
import std.algorithm.mutation : fill;
import std.range : iota;
import deltali.shape.line;
import deltali.util.common_types;
import dsfml.graphics.color;
import graphics.amptext;
import shapes.position;
import shapes.viscurve;
import world.arena.boundary;



class Camp(LR lr) {

private:

    alias VCurve = VisCurve!(BoundaryTopo.Open);

private:

    Array!float m_nubHeights;
    Array!VCurve m_nubs;
    Array!uint m_chordNames;
    Array!AmpText m_visNames;

public:

    this(const Boundary boundary, uint[] chordNames, uint chordThickness) {

        m_chordNames = chordNames.dup();
        m_nubHeights = calcNubHeights(boundary);
        m_nubs = generateNubs(boundary, chordThickness);
        m_visNames = generateVisibleNames(boundary);

    }



    Array!VCurve.Range nubs() {
        return m_nubs[];
    }

    Array!AmpText.Range visNames() {
        return m_visNames[];
    }

private:

    Array!float calcNubHeights(const Boundary boundary) const {

        auto num = m_chordNames.length;
        auto vertSpacing = boundary.height/num;

        auto nubHeights = make!(Array!float)();
        nubHeights.length = num;
        for(auto n=0; n<num; ++n) {
            nubHeights[n] = boundary.top - (n+0.5)*vertSpacing;
        }

        return nubHeights;

    }

    Array!VCurve generateNubs(const Boundary boundary, uint chordThickness) const {

        auto num = m_nubHeights.length;

        static if(lr == LR.Left) {
            auto startX = boundary.left;
            auto endX = boundary.left + 0.05;
        } else {
            auto startX = boundary.right;
            auto endX = boundary.right - 0.05;
        }

        auto nubs = make!(Array!VCurve)();
        nubs.length = num;
        for(auto n=0; n<num; ++n) {

            auto start = Vec2F(startX, m_nubHeights[n]);
            auto end = Vec2F(endX, m_nubHeights[n]);
            auto line = new Line!(Differentiable.Tangents)(start, end);
            nubs[n] = new VCurve(line, Color.Green, chordThickness, false, true);

        }

        return nubs;

    }

    Array!AmpText generateVisibleNames(const Boundary boundary) const {

        auto num = m_nubHeights.length;
        auto horizGap = 0.05f;
        static if(lr == LR.Left) {
            auto x = boundary.left - horizGap;
        } else {
            auto x = boundary.right + horizGap;
        }

        auto visNames = make!(Array!AmpText)();
        visNames.length = num;
        for(auto n=0; n<num; ++n) {

            dstring str = dtext(m_chordNames[n]);
            Position pos = new Position(x, m_nubHeights[n]);
            visNames[n] = new AmpText(str, pos, Color.White);

        }

        return visNames;

    }

}
