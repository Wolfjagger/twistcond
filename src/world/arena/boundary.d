module world.arena.boundary;

import std.math : cos, fmax, sqrt, PI_2;
import dsfml.graphics.color;
import deltali.shape;
import graphics.resolution;
import shapes.viscurve;



class Boundary {

private:

    static float cornerFcn(float x) {
        return sqrt(fmax(cos(x*x/PI_2), 0));
    }

    ICurve!(BoundaryTopo.Closed, Differentiable.Tangents) curve;
    VisCurve!(BoundaryTopo.Closed) visCurve;

    float m_width, m_height;

public:

    @property {
        float width() const { return m_width; }
        float height() const { return m_height; }
        float left() const { return 0.5-width/2; }
        float right() const { return 0.5+width/2; }
        float top() const { return 0.5+height/2; }
        float bot() const { return 0.5-height/2; }
    }


    this(uint thickness,
         float width = 0.8, float height = 0.8, float cornerRadius = 0.2)
    in {
        assert(width <= 1);
        assert(height <= 1);
        assert(cornerRadius <= 0.5);
    } body {

        alias Diff = Differentiable.Tangents;

        m_width = width;
        m_height = height;

        auto res = getCurrentResolution().winRes;
        auto resRatio = cast(float) res.y / res.x;
        curve = new RoundRect!Diff
            (Vec2F(0.5, 0.5), width, height, cornerRadius,
             8, CornerMode.Min, resRatio,
             &cornerFcn);
        visCurve = new VisCurve!(BoundaryTopo.Closed)
            (curve, Color.Blue, thickness);

    }



    VisCurve!(BoundaryTopo.Closed) visual() {
        return visCurve;
    }

    bool intersects(Vec2F start, Vec2F end) const {
        return curve.intersects(start, end);
    }

}
