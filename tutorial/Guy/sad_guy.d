module sad_guy;



public import guy;

import std.typecons;
import shape;
import dsfml.system.vector2;



class sad_guy : guy {

private:

	Unique!shape head;
	Unique!shape leftEye;
	Unique!shape rightEye;
	Unique!shape frown;
	Unique!shape frown_cover;

public:

	this(circle_input head_in,
		 circle_input leftEye_in,
		 circle_input rightEye_in,
		 circle_input frown_in) {

			 head = new shape(head_in);
			 leftEye = new shape(leftEye_in);
			 rightEye = new shape(rightEye_in);
			 frown = new shape(frown_in);

			 rect_input frown_cover_in;
			 frown_cover_in.position = frown_in.position + Vector2f(0, frown_in.radius);
			 frown_cover_in.width = 2*frown_in.radius;
			 frown_cover_in.height = frown_in.radius;
			 frown_cover_in.color = head_in.color;
			 frown_cover = new shape(frown_cover_in);

		 }

	void draw(ref RenderWindow window) {

		head.draw(window);
		leftEye.draw(window);
		rightEye.draw(window);
		frown.draw(window);
		frown_cover.draw(window);

	}

}
