module shape;

import dsfml.system.vector2;
import dsfml.graphics;



struct shape_input(shape.type T : shape.type.Circle) {
	Vector2f position;
	float radius;
	Color color;
}

struct shape_input(shape.type T : shape.type.Rect) {
	Vector2f position;
	float width;
	float height;
	Color color;
}

alias circle_input = shape_input!(shape.type.Circle);
alias rect_input = shape_input!(shape.type.Rect);



class shape {

public:

	enum type {
		Circle, Rect
	}

private:

	type type_shape;
	Shape d_shape;

public:

	this(CircleShape d_shape_init) {
		type_shape = type.Circle;
		d_shape = d_shape_init;
	}

	this(const ref circle_input input) {
		auto circle = new CircleShape(input.radius);
		circle.position = input.position;
		circle.fillColor = input.color;
		this(circle);
	}

	this(RectangleShape d_shape_init) {
		type_shape = type.Rect;
		d_shape = d_shape_init;
	}

	this(const ref rect_input input) {
		auto rect = new RectangleShape(Vector2f(input.width, input.height));
		rect.position = input.position;
		rect.fillColor = input.color;
		this(rect);
	}



	void draw(ref RenderWindow window) {

		window.draw(d_shape);

	}

};
