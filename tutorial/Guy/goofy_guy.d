module goofy_guy;

public import guy;

import std.typecons;
import shape;



class goofy_guy : guy {

private:

	Unique!shape head;
	Unique!shape leftEye;
	Unique!shape rightEye;
	Unique!shape smile;
	Unique!shape smile_cover;

public:

	this(circle_input head_in,
		 circle_input leftEye_in,
		 circle_input rightEye_in,
		 circle_input smile_in) {

			 head = new shape(head_in);
			 leftEye = new shape(leftEye_in);
			 rightEye = new shape(rightEye_in);
			 smile = new shape(smile_in);

			 rect_input smile_cover_in;
			 smile_cover_in.position = smile_in.position;
			 smile_cover_in.width = 2*smile_in.radius;
			 smile_cover_in.height = smile_in.radius;
			 smile_cover_in.color = head_in.color;
			 smile_cover = new shape(smile_cover_in);

		 }

	void draw(ref RenderWindow window) {

		head.draw(window);
		leftEye.draw(window);
		rightEye.draw(window);
		smile.draw(window);
		smile_cover.draw(window);

	}

}
