import std.stdio;
import std.typecons;
import dsfml.graphics;
import shape;
import goofy_guy;
import sad_guy;



void main(string[] argv) {

	const auto window_width = 800;
	const auto window_height = 600;
	auto window = new RenderWindow(VideoMode(window_width, window_height), "Hello DSFML!");

	circle_input head_goofy;
	head_goofy.position = Vector2f(100,75);
	head_goofy.radius = 100;
	head_goofy.color = Color.Green;

	circle_input left_eye_goofy;
	left_eye_goofy.position = Vector2f(145,100);
	left_eye_goofy.radius = 10;
	left_eye_goofy.color = Color.Blue;

	circle_input right_eye_goofy = left_eye_goofy;
	right_eye_goofy.position.x += 4*right_eye_goofy.radius;

	circle_input smile_goofy;
	smile_goofy.position = Vector2f(140,150);
	smile_goofy.radius = 30;
	smile_goofy.color = Color.Red;

	guy goofy = new goofy_guy(head_goofy,
							  left_eye_goofy, right_eye_goofy,
							  smile_goofy);

	circle_input head_cranky = head_goofy;
	head_cranky.position.x =
		window_width - head_goofy.position.x - 2*head_goofy.radius;

	circle_input left_eye_cranky = left_eye_goofy;
	left_eye_cranky.position.x =
		window_width - left_eye_goofy.position.x - 2*left_eye_goofy.radius;

	circle_input right_eye_cranky = right_eye_goofy;
	right_eye_cranky.position.x =
		window_width - right_eye_goofy.position.x - 2*right_eye_goofy.radius;

	circle_input smile_cranky = smile_goofy;
	smile_cranky.position.x =
		window_width - smile_goofy.position.x - 2*smile_goofy.radius;
	smile_cranky.position.y += smile_cranky.radius;

	guy cranky = new sad_guy(head_cranky,
							 left_eye_cranky, right_eye_cranky,
							 smile_cranky);

	while(window.isOpen()) {
		
		Event event;

		while(window.pollEvent(event)) {
			if(event.type == event.EventType.Closed) {
				window.close();
			}
		}

		window.clear();

		goofy.draw(window);
		cranky.draw(window);

		window.display();

	}

}
